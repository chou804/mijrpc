基於 JSON RPC 的 microservices framework

# Requirement

## setup consul

consul 環境 (https://www.consul.io/downloads.html)

可先執行 single node consul 測試

`consul agent -server -data-dir=data - bootstrap-expect=1 -bind=127.0.0.1 -ui`

## dotnet core

專案使用 dotnet core SKD 2.1.3 (https://www.microsoft.com/net/download/windows)

# Examples

## HelloService

首先執行 dotnet restore 將所需的 package 抓下來

`dotnet restore`

建置 HelloService

`dotnet build Examples\HelloService`

啟動

`dotnet Examples\HelloService\bin\Debug\netcoreapp2.1\HelloService.dll`

正常的啟動結果

```bash
> dotnet Examples\HelloService\bin\Debug\netcoreapp2.1\HelloService.dll
Register service=> TROYA-RPC-HELLO_GetSecret:48554 TROYA
Register service=> TROYA-RPC-HELLO_HelloInput:48554 TROYA
Hosting environment: Production
Content root path: C:\dev_test\jrpcservice
Now listening on: http://0.0.0.0:48554
Application started. Press Ctrl+C to shut down.
```

每次啟動 service 的 port 是隨機從 40000 ~ 50000 取得一個可用的 port

如果要指定則可以加 -p or --port 來指定，例如指定 5008 

`dotnet Examples\HelloService\bin\Debug\netcoreapp2.1\HelloService.dll -p 5008`

接下來就可以透過 HTTP POST 來呼叫 HelloService 的服務

```http
POST /jrpc HTTP/1.1
Host: localhost:5008
Content-Type: application/json
Cache-Control: no-cache

{
  "jsonrpc": "2.0",
  "method": "HELLO.HelloInput",
  "params": {
  	"Name":"Troy"
  },
  "id": "243a718a-2ebb-4e32-8cc8-210c39e8a14b"
}
```

## 範例程式碼

```csharp
static void Main(string[] args)
{
    RPCServer.AddService(new HelloImpl());
    RPCServer.Run(args);
}

public class HelloImpl
{
    [RPCFunc]
    public async Task<HELLO.HelloView> Hello(HELLO.HelloInput input)
    {
        var view = new HELLO.HelloView();
        view.Msg = "Hello " + input.Name;

        return await Task.FromResult(view);
    }

    ...
}
```

所有的 service 皆以 console app 的型式來開發，透過 RPCServer.AddService(...) 將有任何有標註 `[RPCFunc]` 的方法的 Class 變成可被呼叫的 jrpc method, 其 method name 是該方法的第一個參數類別名稱, 例如 `HELLO.HelloInput` ，所有的傳入參數必須是如 HELLO.HelloInput 的型式來定義，也就是一個外層類別加上多個內層類別。

```csharp
public class HELLO
{
    public class HelloInput
    {
        public string Name { get; set; }
    }
    public class HelloView
    {
        public string Msg { get; set; }
    }

    ...
}
```
