﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace JRPCProto
{
    public class JClient
    {
        static ConcurrentDictionary<Uri, HttpClient> clientDict = new ConcurrentDictionary<Uri, HttpClient>();
        static TimeSpan DEFAULT_TIMEOUT = TimeSpan.FromSeconds(30);
        const string HTTPSchema = "http://";

        public static async Task<(JRPCError err, TReply reply)> CallAsync<TReply>(object req, TimeSpan? timeout = null, Uri serviceUri = null)
        {
            if (timeout == null)
                timeout = DEFAULT_TIMEOUT;

            try
            {
                var method = ServiceNameHelper.ComposeFuncName(req.GetType());

                if (serviceUri == null)
                {
                    var serviceEndPoint = await ServiceDiscovery.GetServiceAddress(method);
                    RPCF.ThrowIf(string.IsNullOrWhiteSpace(serviceEndPoint), $"service is not available temporarily: {method}");
                    serviceUri = new Uri($"{HTTPSchema}{serviceEndPoint}");
                }

                var jreq = new JRPCRequest
                {
                    Params = new JRaw(RPCPacker.Pack(req)),
                    Method = method
                };

                if (!clientDict.TryGetValue(serviceUri, out HttpClient client))
                {
                    client = new HttpClient
                    {
                        BaseAddress = serviceUri
                    };

                    clientDict.TryAdd(serviceUri, client);
                }

                using (var content = new StringContent(RPCPacker.Pack(jreq), Encoding.UTF8, "application/json"))
                {
                    var response = await client.PostAsync("jrpc", content);
                    response.EnsureSuccessStatusCode();

                    var s = await response.Content.ReadAsStringAsync();
                    var v = RPCPacker.Unpack<JRPCResponse>(s);
                    var r = RPCPacker.Unpack<TReply>(v.Result.ToString());

                    return (v.Error, r);
                }
            }
            catch (Exception ex)
            {
                return (new JRPCError { Message = "執行異常", Data = ex.Message }, default(TReply));
            }
        }

        public static async Task<JRPCResponse> ForwardAsync(JRPCRequest req, TimeSpan? timeout = null, Uri serviceUri = null)
        {
            if (serviceUri == null)
            {
                var serviceEndPoint = await ServiceDiscovery.GetServiceAddress(req.Method);
                RPCF.ThrowIf(string.IsNullOrWhiteSpace(serviceEndPoint), $"service is not available temporarily: {req.Method}");

                serviceUri = new Uri($"{HTTPSchema}{serviceEndPoint}");
            }

            if (!clientDict.TryGetValue(serviceUri, out HttpClient client))
            {
                client = new HttpClient
                {
                    BaseAddress = serviceUri
                };

                clientDict.TryAdd(serviceUri, client);
            }

            using (var content = new StringContent(RPCPacker.Pack(req), Encoding.UTF8, "application/json"))
            {
                var response = await client.PostAsync("jrpc", content);
                response.EnsureSuccessStatusCode();

                var s = await response.Content.ReadAsStringAsync();
                var v = RPCPacker.Unpack<JRPCResponse>(s);
                return v;
            }
        }
    }
}
