﻿using Newtonsoft.Json;
using System.Text;

namespace JRPCProto
{
    public class RPCResponse
    {
        public string PackedData;

        public void Pack(object obj)
        {
            PackedData = RPCPacker.Pack(obj); 
        }

        public T Unpack<T>()
        {
            return RPCPacker.Unpack<T>(PackedData);
        }
    }
}