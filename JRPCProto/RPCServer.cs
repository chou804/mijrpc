﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using System.Reflection;
using Newtonsoft.Json.Linq;
using CommandLine;
using Microsoft.Extensions.Primitives;
using System.Text.RegularExpressions;

namespace JRPCProto
{
    interface IRPCFunc { Task<RPCResponse> ExecuteAsync(RPCRequest request); }

    public class RPCServer
    {
        static Regex rxBearer = new Regex(@"^bearer\s+(.*)$", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        static RPCServer()
        {
            #region graceful shutdown
            AppDomain.CurrentDomain.ProcessExit += (s, ev) => { Shutdown(); };
            Console.CancelKeyPress += (s, ev) => { Shutdown(); ev.Cancel = true; };
            #endregion
        }

        static int PORT;

        private static readonly ConcurrentDictionary<string, IRPCFunc> funcDict = new ConcurrentDictionary<string, IRPCFunc>();

        /// <summary>
        /// store serviceName, serviceId
        /// </summary>
        private static readonly Dictionary<string, string> servicesDict = new Dictionary<string, string>();

        public static void Run(string[] args)
        {
            Parser.Default.ParseArguments<CmdOptions>(args)
                .WithParsed(opt => Start(opt.Port, opt.NoConsul));
        }

        /// <summary>
        /// 啟動Service
        /// </summary>
        /// <param name="port"></param>
        /// <param name="noServiceDiscovery"></param>
        public static void Start(int? port = null, bool noServiceDiscovery = false)
        {
            if (port == null)
                PORT = NetUtil.GetFreePortInRange(40000, 50000);
            else
                PORT = port.Value;

            var host = WebHost.CreateDefaultBuilder()
                .UseUrls($"http://0.0.0.0:{PORT}")
                .ConfigureLogging(logging => { logging.SetMinimumLevel(LogLevel.Warning); })
                .ConfigureServices(services =>
                {
                    services.AddResponseCompression();
                })
                .Configure(app =>
                {
                    app.UseResponseCompression();
                    app.Map("/jrpc", JRPCHandler);
                })
                .Build();

            if (!noServiceDiscovery)
            {
                foreach (var fnName in funcDict.Keys)
                {
                    var serviceName = ServiceNameHelper.ComposeServiceName(fnName);
                    var serviceId = ServiceNameHelper.ComposeServiceId(fnName, PORT);
                    servicesDict.Add(serviceName, serviceId);
                }

                RegisterServiceDiscovery();
            }

            host.Run();
        }

        private static void JRPCHandler(IApplicationBuilder app)
        {
            app.Run(async ctx =>
            {
                ctx.Response.ContentType = "application/json";

                var jres = new JRPCResponse();
                try
                {

                    string body;
                    using (var sr = new StreamReader(ctx.Request.Body, encoding: Encoding.UTF8))
                    {
                        body = sr.ReadToEnd();
                    }

                    var rpcReq = new RPCRequest();
                    rpcReq.JWT = GetJWT(ctx.Request.Headers);
                    try
                    {
                        var jreq = JsonConvert.DeserializeObject<JRPCRequest>(body);
                        rpcReq.RPCFuncName = jreq.Method;
                        rpcReq.PackedData = jreq.Params.ToString();

                        jres.Id = jreq.Id;
                    }
                    catch (Exception e)
                    {
                        jres.Error = new JRPCError(JRPCError.PARSE_ERROR, e.Message);
                        await ctx.Response.WriteAsync(RPCPacker.Pack(jres, true));
                    }

                    var rpcResponse = await ExecuteAsync(rpcReq);

                    jres.Result = new JRaw(rpcResponse.PackedData);
                }
                catch (JRPCException ex)
                {
                    // 預期的錯誤
                    jres.Error = new JRPCError(1, ex.Message, null);
                    ctx.Response.StatusCode = ex.HttpStatusCode;
                }
                catch (JRPCMethodNotFoundException ex)
                {
                    jres.Error = new JRPCError(JRPCError.METHOD_NOT_FOUND, ex.Message);
                }
                catch (Newtonsoft.Json.JsonSerializationException ex)
                {
                    jres.Error = new JRPCError(JRPCError.INVALID_PARAMS, ex.Message);
                }
                catch (Exception ex)
                {
                    jres.Error = new JRPCError(JRPCError.INTERNAL_ERROR, ex.Message);
                }

                await ctx.Response.WriteAsync(RPCPacker.Pack(jres, true));
            });
        }

        #region Obsolete
        /*這是使用 //var host = WebHost.Start($"http://0.0.0.0:{PORT}", router => router.MapPost("jrpc/", JRPCHandlerAsync)); */
        //[Obsolete]
        //static async Task<Task> JRPCHandlerAsync(HttpRequest req, HttpResponse res, RouteData data)
        //{
        //    res.ContentType = "application/json";

        //    var jres = new JRPCResponse();
        //    try
        //    {

        //        string body;
        //        using (var sr = new StreamReader(req.Body, encoding: Encoding.UTF8))
        //        {
        //            body = sr.ReadToEnd();
        //        }

        //        var rpcReq = new RPCRequest();
        //        try
        //        {
        //            var jreq = JsonConvert.DeserializeObject<JRPCRequest>(body);
        //            rpcReq = new RPCRequest
        //            {
        //                RPCFuncName = jreq.Method,
        //                PackedData = jreq.Params.ToString()
        //            };
        //            jres.Id = jreq.Id;
        //        }
        //        catch (Exception e)
        //        {
        //            jres.Error = new JRPCError(JRPCError.PARSE_ERROR, e.Message);
        //            return await Task.FromResult(res.WriteAsync(RPCPacker.Pack(jres, true)));
        //        }

        //        var rpcResponse = await ExecuteAsync(rpcReq);

        //        jres.Result = new JRaw(rpcResponse.PackedData);
        //    }
        //    catch (JSONRPCMethodNotFoundException ex)
        //    {
        //        jres.Error = new JRPCError(JRPCError.METHOD_NOT_FOUND, ex.Message);
        //    }
        //    catch (Newtonsoft.Json.JsonSerializationException ex)
        //    {
        //        jres.Error = new JRPCError(JRPCError.INVALID_PARAMS, ex.Message);
        //    }
        //    catch (Exception ex)
        //    {
        //        jres.Error = new JRPCError(JRPCError.INTERNAL_ERROR, ex.Message);
        //    }

        //    return await Task.FromResult(res.WriteAsync(RPCPacker.Pack(jres, true)));
        //}
        #endregion

        /// <summary>
        /// 新增service物件，將service內含有[RPCFunc]的方法都註冊為Microservice
        /// </summary>
        /// <param name="serviceObj"></param>
        public static void AddService(object serviceObj)
        {
            RegisterFuncs(serviceObj);
        }

        static bool ShuttingDown;
        static void Shutdown(int exitCode = 0)
        {
            if (!ShuttingDown)
            {
                ShuttingDown = true;

                var sd = new ServiceDiscovery();
                foreach (var kv in servicesDict)
                    sd.DeregisterService(kv.Value);

                Console.WriteLine("deregistered all services");
                Environment.Exit(exitCode);
            }
        }

        public static Task<RPCResponse> ExecuteAsync(RPCRequest input)
        {
            if (funcDict.TryGetValue(input.RPCFuncName, out IRPCFunc f))
            {
                return f.ExecuteAsync(input);
            }
            else
            {
                throw new JRPCMethodNotFoundException($"cannot find method: {input.RPCFuncName}");
            }
        }

        static void Add<TRequest, TResponse>(Func<TRequest, Task<TResponse>> func)
        {
            try
            {
                var inputType = typeof(TRequest);
                var methodName = ServiceNameHelper.ComposeFuncName(inputType);

                if (funcDict.ContainsKey(methodName))
                    throw new Exception($"{methodName} 重覆註冊");

                funcDict.TryAdd(methodName, new GeneralRPCFunc<TRequest, TResponse>(new Func<TRequest, Task<TResponse>>(func)));
            }
            catch (Exception ex)
            {
                throw new Exception("註冊RPC Func時異常", ex);
            }
        }

        /// <summary>
        /// 註冊二個參數包含 RPCOptions, 一個回傳Task`1的方法
        /// </summary>
        /// <typeparam name="TRequest"></typeparam>
        /// <typeparam name="TResponse"></typeparam>
        /// <param name="func"></param>
        static void AddWithRPCOptions<TRequest, TResponse>(Func<TRequest, RPCOptions, Task<TResponse>> func)
        {
            try
            {
                var inputType = typeof(TRequest);
                var methodName = ServiceNameHelper.ComposeFuncName(inputType);

                if (funcDict.ContainsKey(methodName))
                    throw new Exception($"{methodName} 重覆註冊");

                funcDict.TryAdd(methodName, new GeneralRPCFunc<TRequest, TResponse>(new Func<TRequest, RPCOptions, Task<TResponse>>(func)));
            }
            catch (Exception ex)
            {
                throw new Exception("註冊RPC Func時異常", ex);
            }
        }

        /// <summary>
        ///  註冊Service類別，含有[JFunc]的方法
        /// </summary>
        /// <param name="svcObj"></param>
        /// <param name="withConsul">是否註冊到consul</param>
        public static void RegisterFuncs(object svcObj)
        {
            foreach (var m in svcObj.GetType().GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
            {
                // 不處理沒標註[RPCFunc]的方法
                if (m.GetCustomAttribute(typeof(RPCFuncAttribute), false) == null)
                    continue;

                //取得該方法的參數
                var parameters = m.GetParameters();

                if (parameters.Length == 0)
                    throw new Exception("所有JFunc都必須要有一個以上的參數");

                if (parameters.Length == 1 && m.ReturnType != typeof(void) && m.ReturnType != typeof(Task))
                {//一個參數及一個回傳值的版本, Eg Task<HelloView> Hello(HelloInput input)

                    var inputType = parameters[0].ParameterType; //Eg HelloInput
                    var viewType = m.ReturnType; //Eg Task<HelloView>

                    //建立方法的 Delegate
                    var funcType = typeof(Func<,>).MakeGenericType(inputType, viewType);
                    var del = Delegate.CreateDelegate(funcType, svcObj, m);

                    //執行 Add<TRequest, TResponse>(Func<TRequest, Task<TResponse>> func)
                    if (viewType.IsGenericType)
                    {
                        if (!viewType.Name.StartsWith("Task") || viewType.GenericTypeArguments?.Length != 1)
                            throw new Exception("方法的回傳值必需是Task or Task<>");

                        var addMethod = typeof(RPCServer).GetMethod(nameof(Add), BindingFlags.NonPublic | BindingFlags.Static)
                            .MakeGenericMethod(inputType, viewType.GenericTypeArguments[0]);
                        addMethod.Invoke(null, new object[] { del });
                    }
                }
                else if (parameters.Length == 2 && m.ReturnType != typeof(void) && m.ReturnType != typeof(Task))
                {//兩個參數及一個回傳值的版本, Eg Task<HelloView> Hello(HelloInput input, RPCOptions opt)

                    var inputType = parameters[0].ParameterType; //Eg HelloInput
                    var RPCOptionsType = parameters[1].ParameterType; // Eg RPCOptions
                    var viewType = m.ReturnType; //Eg Task<HelloView>

                    if (RPCOptionsType != typeof(RPCOptions)) throw new Exception("第二個參數必須是 RPCOptions 型別");

                    //建立方法的 Delegate
                    var funcType = typeof(Func<,,>).MakeGenericType(inputType, RPCOptionsType, viewType);
                    var del = Delegate.CreateDelegate(funcType, svcObj, m);

                    //執行 AddWithRPCOptions<TRequest, TResponse>(Func<TRequest, RPCOptions, Task<TResponse>> func)
                    if (viewType.IsGenericType)
                    {
                        if (!viewType.Name.StartsWith("Task") || viewType.GenericTypeArguments?.Length != 1)
                            throw new Exception("方法的回傳值必需是Task or Task<>");

                        var addMethod = typeof(RPCServer).GetMethod(nameof(AddWithRPCOptions), BindingFlags.NonPublic | BindingFlags.Static)
                            .MakeGenericMethod(inputType, viewType.GenericTypeArguments[0]);
                        addMethod.Invoke(null, new object[] { del });
                    }
                }
                else
                {
                    throw new Exception("RPCFunc 的格式不符");
                }
            }
        }

        static void RegisterServiceDiscovery()
        {
            var sd = new ServiceDiscovery();
            foreach (var kv in servicesDict)
                sd.RegisterService(kv.Key, kv.Value, PORT);
        }

        /// <summary>
        /// 從header取得Authorization，並去掉bearer
        /// </summary>
        /// <param name="headers"></param>
        /// <returns></returns>
        static string GetJWT(IHeaderDictionary headers)
        {
            if (headers.TryGetValue("Authorization", out StringValues jwtValue))
            {
                var m = rxBearer.Match(jwtValue.ToString());
                if (m.Success && m.Groups.Count == 2)
                {
                    return m.Groups[1].Value;
                }
            }
            return "";
        }
    }

    /// <summary>
    /// 回傳Task`1
    /// </summary>
    /// <typeparam name="TInput"></typeparam>
    /// <typeparam name="TReply"></typeparam>
    class GeneralRPCFunc<TInput, TReply> : IRPCFunc
    {
        readonly Func<TInput, Task<TReply>> func;
        readonly Func<TInput, RPCOptions, Task<TReply>> funcWithRPCOptions;

        public GeneralRPCFunc(Func<TInput, Task<TReply>> func)
        {
            this.func = func;
        }

        public GeneralRPCFunc(Func<TInput, RPCOptions, Task<TReply>> func)
        {
            this.funcWithRPCOptions = func;
        }

        public async Task<RPCResponse> ExecuteAsync(RPCRequest request)
        {
            var reply = new RPCResponse();

            if (funcWithRPCOptions != null)
            {
                reply.Pack(await funcWithRPCOptions.Invoke(request.Unpack<TInput>(), new RPCOptions { AuthToken = request.JWT }));
            }
            else
            {
                reply.Pack(await func.Invoke(request.Unpack<TInput>()));
            }

            return reply;
        }
    }
}
