﻿using System;
using System.Collections.Generic;
using System.Text;
using CommandLine;

namespace JRPCProto
{
    public class CmdOptions
    {
        [Option('p', "port", Required = false, HelpText = "specify port to use, or open a free port between 40000 ~ 50000")]
        public int? Port { get; set; }

        [Option('n', "no-consul", Default = false, Required = false, HelpText = "donnot use consul service discovery")]
        public bool NoConsul { get; set; }
    }
}
