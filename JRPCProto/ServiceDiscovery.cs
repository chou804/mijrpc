﻿using Consul;
using DnsClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace JRPCProto
{
    public class ServiceDiscovery
    {
        ConsulClient consul;

        public ServiceDiscovery()
        {
            consul = CreateConsulClient();
        }

        /// <summary>
        /// 註冊 service 到 consul
        /// </summary>
        /// <param name="serviceName"></param>
        /// <param name="serviceId"></param>
        /// <param name="port"></param>
        public void RegisterService(string serviceName, string serviceId, int port)
        {
            //找出相同serviceName已經死掉的, 將他從consul刪除
            var health = this.consul.Health.Service(serviceName).Result;
            foreach (var h in health.Response)
            {
                var check = h.Checks.FirstOrDefault(
                    t => t.Status.Status.ToLower() == "critical" &&
                    t.Output.Contains("No connection could be made"));

                if (check != null)
                {
                    Console.WriteLine($"{check.ServiceID} has dead");
                    this.consul.Agent.ServiceDeregister(check.ServiceID).Wait();
                    Console.WriteLine($"{check.ServiceID} deregistered");
                }
            }

            var reg = new AgentServiceRegistration
            {
                ID = serviceId,
                Name = serviceName,
                Port = port,
                Tags = new string[] { System.Net.Dns.GetHostName(), port.ToString() },
                Check = new AgentServiceCheck
                {
                    TCP = "localhost:" + port.ToString(),
                    Interval = TimeSpan.FromSeconds(5),
                    Timeout = TimeSpan.FromSeconds(30),
                    Status = HealthStatus.Passing
                }
            };

            consul.Agent.ServiceRegister(reg).Wait();
            Console.WriteLine("Register service=> {0} {1}", reg.ID, reg.Tags[0]);
        }

        /// <summary>
        /// 將 Service 從 consul 刪除
        /// </summary>
        /// <param name="serviceId"></param>
        public void DeregisterService(string serviceId)
        {
            this.consul.Agent.ServiceDeregister(serviceId).Wait();

            Console.WriteLine($"{serviceId} is deregistered from consul");
        }

        public static async Task<string> GetServiceAddress(string fnName)
        {
            var serviceName = ServiceNameHelper.ComposeServiceName(fnName);
            var endpoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8600);
            var dnsclient = new LookupClient(endpoint) { UseCache = true, Timeout = TimeSpan.FromSeconds(5) };

            string serviceAddr = "";

            try
            {
                //建立client向consul問該service可連接的ip位址
                var dnsresult = await dnsclient.ResolveServiceAsync("service.consul", serviceName).ConfigureAwait(false);
                if (dnsresult.Count() == 0)
                {//找不到該service的註冊資訊
                    return serviceAddr;
                }

                var addr = dnsresult.First().AddressList.FirstOrDefault();
                var port = dnsresult.First().Port;
                serviceAddr = $"{addr}:{port}";
            }
            catch (DnsResponseException ex)
            {
                //無法連接本機的consul
                throw new Exception("無法連接consul", ex);
            }
            catch (Exception ex)
            {
                //未知錯誤
                throw new Exception($"取得服務位址異常, serviceName:{serviceName}", ex);
            }

            return serviceAddr;
        }

        #region Consul
        private ConsulClient CreateConsulClient()
        {
            Uri uri;
            string consulUri = System.Environment.GetEnvironmentVariable("consulUri");
            if (string.IsNullOrWhiteSpace(consulUri))
                uri = new Uri("http://127.0.0.1:8500", UriKind.Absolute);
            else
                uri = new Uri(consulUri, UriKind.Absolute);

            consul = new ConsulClient(conf =>
            {
                conf.Token = GetToken();
                conf.Address = uri;
            });
            return consul;
        }
        private string GetToken()
        {
            string token = "";
            try { token = GetTokenFromEnvVariable(); } catch { }

            if (string.IsNullOrWhiteSpace(token))
                token = GetTokenFromAppSetting();

            return token;
        }

        private string GetTokenFromEnvVariable()
        {
            return Environment.GetEnvironmentVariable("consul_token");
        }

        private string GetTokenFromAppSetting()
        {
            return AppSettingsHelper.Get("consul_token");
        }
        #endregion
    }
}
