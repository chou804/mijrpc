﻿using System;

namespace JRPCProto
{
    [AttributeUsage(AttributeTargets.Method, Inherited = false)]
    public sealed class RPCFuncAttribute : Attribute
    {
    }
}