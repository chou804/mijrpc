﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JRPCProto
{
    public class RPCF
    {
        public static void ThrowIf(bool condition, string msg, int httpStatusCode = 400, Exception ex = null)
        {
            if (condition)
            {
                if (ex != null)
                    throw new JRPCException(msg, ex, httpStatusCode);
                else
                    throw new JRPCException(msg, httpStatusCode);
            }
        }
    }
}
