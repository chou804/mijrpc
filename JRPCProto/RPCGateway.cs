﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace JRPCProto
{
    public class RPCGateway
    {
        public static void ForwardHandler(IApplicationBuilder app)
        {
            app.Run(async ctx =>
            {
                ctx.Request.Headers["Accept-Encoding"] = "gzip";
                ctx.Response.ContentType = "application/json";

                var jres = new JRPCResponse();
                try
                {

                    string body;
                    using (var sr = new StreamReader(ctx.Request.Body, encoding: Encoding.UTF8))
                    {
                        body = sr.ReadToEnd();
                    }

                    var jreq = new JRPCRequest();
                    try
                    {
                        jreq = RPCPacker.Unpack<JRPCRequest>(body);
                    }
                    catch (Exception e)
                    {
                        jres.Error = new JRPCError(JRPCError.PARSE_ERROR, e.Message);
                        await ctx.Response.WriteAsync(RPCPacker.Pack(jres, true));
                    }

                    jres = await JClient.ForwardAsync(jreq);
                }
                catch (JRPCException ex)
                {
                    // 預期的錯誤
                    jres.Error = new JRPCError(1, ex.Message, null);
                    ctx.Response.StatusCode = ex.HttpStatusCode;
                }
                catch (JRPCMethodNotFoundException ex)
                {
                    jres.Error = new JRPCError(JRPCError.METHOD_NOT_FOUND, ex.Message);
                }
                catch (Newtonsoft.Json.JsonSerializationException ex)
                {
                    jres.Error = new JRPCError(JRPCError.INVALID_PARAMS, ex.Message);
                }
                catch (Exception ex)
                {
                    jres.Error = new JRPCError(JRPCError.INTERNAL_ERROR, ex.Message);
                }

                await ctx.Response.WriteAsync(RPCPacker.Pack(jres, true));
            });
        }
    }
}
