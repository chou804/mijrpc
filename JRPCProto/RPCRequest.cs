﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace JRPCProto
{
    public class RPCRequest
    {
        public string PackedData;
        public string RPCFuncName;
        public string JWT { get; set; }

        public void Pack(Object obj)
        {
            RPCFuncName = ServiceNameHelper.ComposeFuncName(obj.GetType());
            PackedData = RPCPacker.Pack(obj);
        }

        public T Unpack<T>()
        {
            return RPCPacker.Unpack<T>(PackedData);
        }
    }
}
