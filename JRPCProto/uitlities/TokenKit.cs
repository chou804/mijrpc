﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace JRPCProto
{
    public class TokenKit
    {
        static readonly JwtHeader header;
        static readonly JwtSecurityTokenHandler handler;
        static readonly SigningCredentials credentials;
        static readonly SymmetricSecurityKey secretKey;

        static TokenKit()
        {
            const string key = "xkBuE+d4kOksnps76pujP3ZhC7zHWG++SvuASQF39GCO/Fnu+qzOzXxL5Pv/cgLgId5gRoFtUZ1TxUIkO/N43Q==";
            secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            credentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256Signature);
            header = new JwtHeader(credentials);
            handler = new JwtSecurityTokenHandler();
        }

        public static string CreateToken(string username)
        {
            //var payload = new JwtPayload
            //{
            //   { "some ", "hello "},
            //   { "scope", "http://dummy.com/"},
            //};

            var descriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Name, username)
                }),
                IssuedAt = DateTime.UtcNow,
                Expires = DateTime.UtcNow.AddDays(365),
                SigningCredentials = credentials
            };

            //var secToken = new JwtSecurityToken(header, payload);
            var secToken = handler.CreateJwtSecurityToken(descriptor);
            secToken.Payload["test1"] = "test1"; //額外要存的欄位
            var tokenString = handler.WriteToken(secToken);

            return tokenString;
        }

        public static ClaimsPrincipal GetPrincipal(string token)
        {
            try
            {
                var jwtToken = (JwtSecurityToken)handler.ReadToken(token);
                if (jwtToken == null)
                    return null;

                var parameters = new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = secretKey
                };
                var principal = handler.ValidateToken(token, parameters, out SecurityToken securityToken);
                return principal;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static string ValidateToken(string token)
        {
            string username = null;
            var principal = GetPrincipal(token);
            if (principal == null)
                return null;
            ClaimsIdentity identity = null;
            try
            {
                identity = (ClaimsIdentity)principal.Identity;
            }
            catch (NullReferenceException)
            {
                return null;
            }
            var usernameClaim = identity.FindFirst(ClaimTypes.Name);
            username = usernameClaim.Value;
            return username;
        }

        public static void ReadToken(string tokenString)
        {
            // And finally when  you received token from client
            // you can  either validate it or try to  read

            var token = handler.ReadJwtToken(tokenString);

            Console.WriteLine(token.Payload.First().Value);
        }


        /// <summary>
        /// 產生一個key
        /// </summary>
        public static string MakeKey()
        {
            using (var hMACSHA256 = new System.Security.Cryptography.HMACSHA256())
            {
                return Convert.ToBase64String(hMACSHA256.Key);
            }
        }
    }
}
