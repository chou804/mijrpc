﻿using Microsoft.Extensions.Configuration;
using System.IO;

namespace JRPCProto
{
    public class AppSettingsHelper
    {
        static IConfiguration conf { get; set; }

        static AppSettingsHelper()
        {
            conf = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: true)
            .Build();
        }

        public static string Get(string key)
        {
            return conf[key];
        }
    }
}
