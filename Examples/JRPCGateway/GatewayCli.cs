﻿using JRPCProto;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net.Http;
using System.Text;

namespace JRPCGateway
{
    class GatewayCli
    {
        static void Main(string[] args)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("hosting.json", optional: true)
                .AddCommandLine(args)
                .Build();

            var host = WebHost.CreateDefaultBuilder()
               .UseConfiguration(config)
               .ConfigureServices(services =>
               {
                   services.AddResponseCompression();
               })
               .Configure(app =>
               {
                   app.UseResponseCompression();
                   app.Map("/jrpc", RPCGateway.ForwardHandler);
               })
               .Build();

            host.Run();
        }
    }
}
