﻿using JRPCProto;
using ServiceModel.RPC;
using System;
using System.Threading.Tasks;

namespace AccountService
{
    class AccountCli
    {
        static void Main(string[] args)
        {
            RPCServer.AddService(new AccountImpl());
            RPCServer.Run(args);
        }
    }

    public class AccountImpl
    {
        [RPCFunc]
        public async Task<ACCOUNT.LoginView> Login(ACCOUNT.Login input)
        {
            var v = new ACCOUNT.LoginView();

            //TODO 登入驗證

            // call another service via service discovery
            var (e, v2) = await JClient.CallAsync<HELLO.HelloView>(new HELLO.HelloInput { Name = "Troy2" });
            Console.WriteLine(v2.Msg);

            v.Account = input.Account;
            v.Auth_token = TokenKit.CreateToken(input.Account);

            return await Task.FromResult(v);
        }
    }
}
