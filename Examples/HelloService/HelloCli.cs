﻿using JRPCProto;
using ServiceModel.RPC;
using System;
using System.Threading.Tasks;

namespace HelloService
{
    class HelloCli
    {
        static void Main(string[] args)
        {
            RPCServer.AddService(new HelloImpl());
            RPCServer.Run(args);
        }

        public class HelloImpl
        {
            [RPCFunc]
            public async Task<HELLO.HelloView> Hello(HELLO.HelloInput input)
            {
                var view = new HELLO.HelloView();
                view.Msg = "Hello " + input.Name;

                return await Task.FromResult(view);
            }


            [RPCFunc]
            public async Task<HELLO.GetSecretView> GetSecret(HELLO.GetSecret input, RPCOptions opt)
            {
                var view = new HELLO.GetSecretView();

                RPCF.ThrowIf(string.IsNullOrWhiteSpace(opt.AuthToken), "使用者未登入", 401);

                Console.WriteLine(opt.AuthToken);

                view.Msg = "SECRET " + input.Name;

                return await Task.FromResult(view);
            }
        }
    }
}
