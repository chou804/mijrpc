﻿namespace ServiceModel.RPC
{
    public class ACCOUNT
    {
        public class Login
        {
            public string Account { get; set; }
            public string Password { get; set; }
        }
        public class LoginView
        {
            public string UserId { get; set; }
            public string Account { get; set; }
            public string Name { get; set; }
            public string PictureUrl { get; set; }
            public string Auth_token { get; set; }
        }
    }
}
