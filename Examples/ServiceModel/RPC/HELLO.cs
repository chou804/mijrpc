﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceModel.RPC
{
    public class HELLO
    {
        public class HelloInput
        {
            public string Name { get; set; }
        }
        public class HelloView
        {
            public string Msg { get; set; }
        }

        public class GetSecret
        {
            public string Name { get; set; }
        }
        public class GetSecretView
        {
            public string Msg { get; set; }
        }
    }
}
